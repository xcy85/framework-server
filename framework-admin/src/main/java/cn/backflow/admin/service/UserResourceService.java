package cn.backflow.admin.service;

import cn.backflow.admin.entity.User;
import cn.backflow.admin.entity.UserResource;
import cn.backflow.admin.repository.UserResourceRepository;
import cn.backflow.data.service.AbstractService;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserResourceService extends AbstractService<UserResource, Integer> {

    private final UserResourceRepository userResourceRepo;

    @Autowired
    public UserResourceService(UserResourceRepository userResourceRepo) {this.userResourceRepo = userResourceRepo;}

    public List<Map<String, Object>> findByResourceTypeAndResourceId(String resourceType, Integer resourceId) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("resourceType", resourceType);
        parameter.put("resourceId", resourceId);
        return userResourceRepo.findByParameter(parameter);
    }


    /**
     * 资源分配
     *
     * @param o        资源对象
     * @param targets  分配目标
     * @param assigner 分配者
     */
    @Transactional
    public int assign(Object o, List<Integer> targets, User assigner) throws Exception {
        Integer resource_id = (Integer) MethodUtils.invokeMethod(o, "getId"); // 资源ID
        String resource_type = o.getClass().getSimpleName();

        deleteByResourceTypeAndResourceId(resource_type, resource_id);
        if (targets.isEmpty()) {
            return 0;
        }

        List<UserResource> urs = new ArrayList<>();
        for (Integer user : targets) {
            UserResource ur = new UserResource();
            ur.setUserId(user);
            ur.setResourceType(resource_type);
            ur.setResourceId(resource_id);
            urs.add(ur);
        }
        return userResourceRepo.insertBatch(urs);
    }

    @Transactional
    public int deleteByResourceTypeAndResourceId(String resourceType, Integer resourceId) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("resourceType", resourceType);
        parameter.put("resourceId", resourceId);
        return deleteByParameter(parameter);
    }

    @Transactional
    public int deleteByParameter(Object parameter) throws DataAccessException {
        return userResourceRepo.deleteByParameter(parameter);
    }

}