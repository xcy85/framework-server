package cn.backflow.admin.repository;

import cn.backflow.admin.entity.Dict;
import cn.backflow.data.repository.BaseMyBatisRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository
public class DictRepository extends BaseMyBatisRepository<Dict, Integer> {

    /**
     * 查询返回
     *
     * @param code   字典编码
     * @param mapKey map对象的key属性
     * @return Map&lt;mapKey, Dict&gt; 对象
     */
    public Map<Comparable, Dict> findMapByCode(String code, String mapKey) {
        return sqlSession.selectMap("Dict.findByCode", code, mapKey);
    }

    public List<Dict> findByCode(String code) {
        return sqlSession.selectList("Dict.findByCode", code);
    }

    public int insertBatch(Collection<Dict> dicts) {
        return sqlSession.insert("Dict.insertBatch", dicts);
    }

    public int deleteByCode(String code) {
        return sqlSession.delete("Dict.deleteByCode", code);
    }
}