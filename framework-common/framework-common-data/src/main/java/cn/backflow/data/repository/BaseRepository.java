package cn.backflow.data.repository;

import cn.backflow.data.entity.BaseEntity;
import cn.backflow.data.pagination.Page;
import cn.backflow.data.pagination.PageRequest;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface BaseRepository<E extends BaseEntity, PK extends Serializable> {

    /**
     * 执行SQL查询语句
     */
    Object select(String sql);

    /**
     * 检查某属性是否唯一
     */
    boolean isUnique(String uniqueProperty);

    /**
     * 按主键获取对象
     */
    E getById(PK id);

    /**
     * 查询返回所数据库中所有对象
     */
    List<E> findAll(Object parameter);

    /**
     * 查询返回Map映射
     */
    <T> Map<T, E> findMap(Object parameter, String mapKey);

    /**
     * 分页查询
     */
    Page<E> findByPageRequest(PageRequest pr);

    /**
     * 插入数据
     */
    int insert(E entity);

    /**
     * 批量插入
     */
    int insertBatch(List<E> entities);

    /**
     * 更新数据
     */
    int update(E entity);

    /**
     * 批量选择性更新
     */
    int updateSelective(E entity);

    /**
     * 批量更新
     */
    int updateBatch(List<E> entities);

    /**
     * 批量更新
     */
    int updateSelectiveBatch(List<E> entities);

    /**
     * 根据id检查是否插入或是更新数据
     */
    int saveOrUpdate(E entity);

    /**
     * 按ID删除数据
     */
    int deleteById(PK id);

    /**
     * 批量删除数据
     */
    int deleteBatch(Collection<PK> pks);

    /**
     * 用于hibernate.flush() 有些dao实现不需要实现此类
     */
    void flush();
}