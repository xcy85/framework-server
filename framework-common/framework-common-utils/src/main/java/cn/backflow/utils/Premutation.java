package cn.backflow.utils;

import java.util.Arrays;

import static cn.backflow.utils.Combination.factorial;

public class Premutation {

    /**
     * 排列选择（从列表中选择n个排列）
     *
     * @param dataList 待选列表
     * @param n        选择个数
     */
    public static void premutation(String[] dataList, int n) {
        System.out.println(String.format("A(%d, %d) = %d", dataList.length, n, premutation(dataList.length, n)));
        premutation(dataList, new String[n], 0);
    }

    /**
     * 排列选择
     *
     * @param dataList    待选列表
     * @param resultList  前面（resultIndex-1）个的排列结果
     * @param resultIndex 选择索引，从0开始
     */
    private static void premutation(String[] dataList, String[] resultList, int resultIndex) {
        int resultLen = resultList.length;
        if (resultIndex >= resultLen) { // 全部选择完时，输出排列结果
            System.out.println(Arrays.asList(resultList));
            return;
        }

        // 递归选择下一个
        for (int i = 0; i < dataList.length; i++) {
            // 判断待选项是否存在于排列结果中
            boolean exists = false;
            for (int j = 0; j < resultIndex; j++) {
                if (dataList[i].equals(resultList[j])) {
                    exists = true;
                    break;
                }
            }
            if (!exists) { // 排列结果不存在该项，才可选择
                resultList[resultIndex] = dataList[i];
                premutation(dataList, resultList, resultIndex + 1);
            }
        }
    }

    /**
     * 计算排列数，即A(m, n) = n!/(n-m)!
     */
    public static long premutation(int m, int n) {
        return (m >= n) ? factorial(m) / factorial(m - n) : 0;
    }

}
